package wallet;

import java.util.*;

public class Wallet {

    List<Stock> stocks;
    public Wallet(Stock... stock) {
        this.stocks = Arrays.asList(stock);
    }



    Amount amountIn(Currency target, RateProvider rateProvider) {
        Amount sum = new Amount(target, 0.0);

        for (Stock stock: stocks) {
            ExchangeRate rate = rateProvider.rateFor(stock.type, target);
            Amount amount = stock.apply(rate);
            sum = sum.plus(amount);
        }
        return sum;
    }
}
