package wallet.api;

import okhttp3.HttpUrl;

public interface RestClient {
    String get(HttpUrl url);
}
