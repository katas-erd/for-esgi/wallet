package wallet.api;

record ApiResponse(
        boolean success,
        QueryResponse query,
        InfoResponse info,
        String date,
        double result
) {
}
