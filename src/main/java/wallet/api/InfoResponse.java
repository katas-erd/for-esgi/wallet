package wallet.api;

record InfoResponse(
        long timestamp,
        double rate
) {
}
