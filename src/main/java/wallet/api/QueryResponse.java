package wallet.api;

record QueryResponse(
        String from,
        String to,
        double amount
) {
}
