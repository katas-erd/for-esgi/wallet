package wallet.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import wallet.Currency;
import wallet.ExchangeRate;
import wallet.RateProvider;
import wallet.Type;

public class RestRateProvider implements RateProvider {

    wallet.api.RestClient client;

    public RestRateProvider(wallet.api.RestClient client) {
        this.client = client;
    }

    private final String BASE_URL = "https://api.apilayer.com/exchangerates_data/convert?";

    private Double getConversionRate(String from, String to) {
        var url = HttpUrl.parse(BASE_URL).newBuilder()
                .addQueryParameter("to", to)
                .addQueryParameter("from", from)
                .addQueryParameter("amount", "1")
                .addQueryParameter("apikey", "B8nCHpuqnyApnfQQJTRObcvIMlbQpawh")
                .build();


        String response = client.get(url);
        System.out.println(response);
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            ApiResponse entity = objectMapper.readValue(response, ApiResponse.class);
            return entity.result();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
    @Override
    public ExchangeRate rateFor(Type source, Currency target) {
        String sourceStr = source.name();
        String targetStr = target.name();
        return new ExchangeRate(source, target, getConversionRate(sourceStr, targetStr));
    }
}


