package wallet.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;

public class HttpRestClient implements RestClient {

    @Override
    public String get(HttpUrl url) {
        Request request = new Request.Builder()
                .url(url)
                .build();

        OkHttpClient client = new OkHttpClient();
        try {
            ResponseBody responseBody = client.newCall(request).execute().body();
            return responseBody.string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
