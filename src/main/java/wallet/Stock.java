package wallet;

public class Stock {
    Type type;
    double quantity;

    public Stock(Type type, double quantity) {
        this.type = type;
        this.quantity = quantity;
    }

    public Amount apply(ExchangeRate rate) {
        return rate.apply(quantity);
    }
}
