package wallet;

public record Amount(Currency currency, double value) {



    public Amount plus(Amount other) {
        assert currency == other.currency;

        return new Amount(currency, value + other.value);
    }
}
