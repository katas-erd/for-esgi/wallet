package wallet;

public interface RateProvider {
    ExchangeRate rateFor(Type source, Currency target);
}
