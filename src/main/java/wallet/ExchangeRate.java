package wallet;

public record ExchangeRate(Type source, Currency target, double value) {
    public Amount apply(double quantity) {
        return new Amount(target, quantity * value);
    }
}
