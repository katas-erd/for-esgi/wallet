package wallet.api;

import okhttp3.HttpUrl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import wallet.Currency;
import wallet.ExchangeRate;
import wallet.Type;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


class FakeRestHttpClient implements RestClient {

    String rate;

    public FakeRestHttpClient(String rate) {
        this.rate = rate;
    }

    @Override
    public String get(HttpUrl url) {
        return """
                {
                    "success": true,
                    "query": {
                        "from": "USD",
                        "to": "EUR",
                        "amount": 1
                    },
                    "info": {
                        "timestamp": 1702627623,
                        "rate": $rate
                    },
                    "date": "2023-12-15",
                    "result": $rate
                }
               """.replace("$rate", this.rate);
    }
}

public class RestRateProviderTest {
    @Test
    @DisplayName("Should extract the exchange rate from response")
    void test1() {
        RestRateProvider provider = new RestRateProvider(
                new FakeRestHttpClient("0.1234")
        );

        assertThat(provider.rateFor(Type.USD, Currency.EUR)).isEqualTo(new ExchangeRate(Type.USD, Currency.EUR, 0.1234));
    }

    @Test
    @DisplayName("Should throw an error if the response is malformated")
    void test2() {
        RestRateProvider provider = new RestRateProvider(
                new FakeRestHttpClient("abcde")
        );

        assertThat(provider.rateFor(Type.USD, Currency.EUR)).isEqualTo(new ExchangeRate(Type.USD, Currency.EUR, 0.1234));
    }
}
