package wallet;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import wallet.Amount;
import wallet.Currency;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.filter;

public class AmountTest {
    @Test
    @DisplayName("Assert equality")
    public void test1() {
        assertThat(new Amount(Currency.EUR, 10))
                .isEqualTo(new Amount(Currency.EUR, 10));
    }

    @Test
    @DisplayName("Should sum amounts")
    public void test2() {
        assertThat(
                new Amount(Currency.EUR, 10).plus(new Amount(Currency.EUR, 5))
        ).isEqualTo(new Amount(Currency.EUR, 15));
    }

    @Test
    @DisplayName("Should failed to sum amounts with different currencies")
    public void test3() {

        try {
            new Amount(Currency.EUR, 10).plus(new Amount(Currency.USD, 5));
            Assertions.fail();
        } catch (AssertionError e) {
        }
    }
}
