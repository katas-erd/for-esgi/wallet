package wallet;

import java.util.ArrayList;
import java.util.List;

public class FakeRateProvider implements RateProvider {

    List<ExchangeRate> rates = new ArrayList<>();

    @Override
    public ExchangeRate rateFor(Type source, Currency target) {
        /*if ((source == wallet.Type.USD) && target == wallet.Currency.EUR) {
            return new wallet.ExchangeRate(source, target, 0.9);
        }*/
        for (ExchangeRate rate: rates) {
            if ((rate.source() == source) && rate.target() == target) {
                return rate;
            }
        }
        return new ExchangeRate(source, target, 1.0);
    }

    FakeRateProvider with(Type type, Currency currency, double v) {
        rates.add(new ExchangeRate(type, currency, v));
        return this;
    }
}
