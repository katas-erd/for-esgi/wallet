package wallet;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class WalletTest {

    FakeRateProvider fakeProvider = new FakeRateProvider();
    @Test
    @DisplayName("Given an empty wallet, when we ask for its amount in EUR, then it should be EUR 0")
    public void test1() {
        Wallet myWallet = new Wallet();
        assertThat(myWallet.amountIn(Currency.EUR, fakeProvider))
                .isEqualTo(new Amount(Currency.EUR, 0.0));
    }

    @Test
    @DisplayName("Given a wallet containing EUR 10, when we ask for its amount in EUR, then it should be EUR 10")
    public void test2() {
        Wallet myWallet = new Wallet(new Stock(Type.EUR, 10.0));
        assertThat(myWallet.amountIn(Currency.EUR, fakeProvider))
                .isEqualTo(new Amount(Currency.EUR, 10.0));
    }

    @Test
    @DisplayName("Given a wallet containing EUR 10 and EUR 5.80, when we ask for its amount in EUR, then it should be EUR 15.80")
    public void test3() {
        Wallet myWallet = new Wallet(new Stock(Type.EUR, 10.0), new Stock(Type.EUR, 5.80));
        assertThat(myWallet.amountIn(Currency.EUR, fakeProvider))
                .isEqualTo(new Amount(Currency.EUR, 15.80));
    }

    @Test
    @DisplayName("Given a wallet containing USD 10 and the rate USD -> EUR is 0.9, when we ask for its amount in EUR, then it should be EUR 0.9")
    public void test4() {
        Wallet myWallet = new Wallet(new Stock(Type.USD, 10.0));
        assertThat(myWallet.amountIn(Currency.EUR, fakeProvider.with(Type.USD, Currency.EUR, 0.9)))
                .isEqualTo(new Amount(Currency.EUR, 9.0));
    }

    @Test
    @DisplayName("Given a wallet containing EUR 20, USD 10 and BTC 0.5 & the rate USD -> EUR is 0.9 and the rate BTC -> EUR is 40000, when we ask for its amount in EUR, then it should be EUR 20029")
    public void test5() {
        Wallet myWallet = new Wallet(
         new Stock(Type.EUR, 20.0),
                new Stock(Type.USD, 10.0),
                new Stock(Type.BTC, 0.5)
        );
        assertThat(myWallet.amountIn(Currency.EUR,
                fakeProvider.with(Type.USD, Currency.EUR, 0.9)
                                       .with(Type.BTC, Currency.EUR, 40000)))
                .isEqualTo(new Amount(Currency.EUR, 20029.0));
    }
}


